var io = require('socket.io')(8000);
var Redis = require('ioredis');

function logMessage(message) {
    console.log((new Date()).toISOString() + ' - ' + message);
}

function createRedisClient() {
    return new Redis(6379, 'redis');
}

var redisPersistence = createRedisClient();

var redisLocationSubscriber = createRedisClient();
redisLocationSubscriber.on('message', function(channel, message) {
    const messageData = JSON.parse(message);
    redisPersistence.get('session:' + messageData.sessionId, function(err, id) {
        if (id != null) {
            redisPersistence.hmset('location:' + id, messageData.location);
        }
    });
});
redisLocationSubscriber.subscribe('location');

// console.log('started');

io.on('connection', function(socket) {
    logMessage('connected: ' + socket.client.id);

    var redisLocalPublisher = createRedisClient();

    var redisLocalSubscriber = createRedisClient();
    redisLocalSubscriber.on('message', function(channel, message) {
        const messageData = JSON.parse(message);
        redisPersistence.get('session:' + messageData.sessionId, async function(err, id) {
            if (id != null) {
                var name = await redisPersistence.hget('location:' + id, 'name');
                var location = {
                    id: id,
                    name: name,
                    location: messageData.location
                };
                var locations = [location];
                // console.log(socket.client.id + ' <------- ' + JSON.stringify(locations));
                socket.emit('locationUpdates', locations);
            }
        });
    });

    socket.on('disconnect', function() {
        logMessage('disconnected: ' + socket.client.id);
        redisLocalSubscriber.unsubscribe('location');
        redisPersistence.del('session:' + socket.client.id);
    });
    socket.on('subscribe', function() {
        logMessage('subscribed: ' + socket.client.id);
        redisPersistence.keys('location:*', async function(err, res) {
            var locations = await Promise.all(res.map(async (key) => {
                var locationData = await redisPersistence.hgetall(key);
                return {
                    id: locationData['id'],
                    name: locationData['name'],
                    location: {
                        lat: locationData['lat'],
                        lon: locationData['lon']
                    }
                };
            }));
            // console.log(socket.client.id + ' <------- ' + JSON.stringify(locations));
            socket.emit('locationUpdates', locations);
        });
        redisLocalSubscriber.subscribe('location');
    });
    socket.on('unsubscribe', function() {
        logMessage('unsubscribed: ' + socket.client.id);
        redisLocalSubscriber.unsubscribe('location');
    });
    socket.on('introduce', function(data) {
        logMessage('introduced (' + socket.client.id + '): ' + data['id'] + ', ' + data['name']);
        redisPersistence.set('session:' + socket.client.id, data['id']);
        redisPersistence.hset('location:' + data['id'], 'id', data['id'], 'name', data['name']);
    });
    socket.on('location', function(data) {
        // console.log('location: ' + socket.client.id + ' (' + data['lat'] + ', ' + data['lon'] + ')');
        const messageData = {
            sessionId: socket.client.id,
            location: {
                lat: data['lat'],
                lon: data['lon']
            }
        };
        redisLocalPublisher.publish('location', JSON.stringify(messageData));
    });
});
