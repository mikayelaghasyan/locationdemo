#!/usr/bin/env bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/mikayelaghasyan/locationdemo .
docker push registry.gitlab.com/mikayelaghasyan/locationdemo

scp -i ~/Documents/Projects/BlackSwanPicks/Keys/bsp-key.pem docker-compose.yml ec2-user@34.205.216.212:/home/ec2-user/locationdemo/docker-compose.yml
ssh -i ~/Documents/Projects/BlackSwanPicks/Keys/bsp-key.pem ec2-user@34.205.216.212 << EOF
    cd ~/locationdemo/
    docker login registry.gitlab.com
    docker-compose -f docker-compose.yml pull
    docker-compose -f docker-compose.yml up -d
EOF
